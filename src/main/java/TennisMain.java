import org.apache.log4j.Logger;

import adbi.tennis.entities.Player;
import adbi.tennis.service.Game;
import adbi.tennis.service.Match;
import adbi.tennis.service.Set;


public class TennisMain {

	final static Logger logger = Logger.getLogger(TennisMain.class);

	public static void main(String[] args) {
		Player joueur1 = new Player("Joueur1");
		Player joueur2 = new Player("Joueur2");
		Game game = new Game(joueur1, joueur2);
		Set set = new Set(game);
		Match match = new Match(set);
		match.playMatch();
		logger.info(match.getSets().toString());
		logger.info("Gagnant: " + match.getWinner().toString());
	}

}
