package adbi.tennis.service;

import org.apache.log4j.Logger;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@NoArgsConstructor
public class Set {
	final static Logger logger = Logger.getLogger(Set.class);
	private String score;
	private Game jeu;

	public Set(Game game) {
		this.score = "0 - 0";
		this.jeu = game;
	}

	public void playSet() {
			// play set when the players won games is less then 6
			while (this.jeu.getJoueur1().getNbrJeuGagne() < 6 && this.jeu.getJoueur2().getNbrJeuGagne() < 6 ) {
				this.jeu.playGame();
			}
			// execute this if one of the players won 6 games in a set and no one won the set
			thisGame:while (Math.abs(this.jeu.getJoueur1().getNbrJeuGagne()-this.jeu.getJoueur2().getNbrJeuGagne())<2 ) {
				//if the two players have both won 6 games play a tie break game
				if(this.jeu.getJoueur1().getNbrJeuGagne()==this.jeu.getJoueur2().getNbrJeuGagne()&& this.jeu.getJoueur2().getNbrJeuGagne()==6) {
					this.jeu.playTieBreakGame();
					break thisGame;
				}else {
					this.jeu.playGame();
				}
			}
			
			if (this.getJeu().getJoueur1().getNbrJeuGagne() > this.jeu.getJoueur2().getNbrJeuGagne())
				this.jeu.getJoueur1().setNbrSetGagne(this.jeu.getJoueur1().getNbrSetGagne() + 1);
			else
				this.jeu.getJoueur2().setNbrSetGagne(this.jeu.getJoueur2().getNbrSetGagne() + 1);

			this.score = String.valueOf(this.jeu.getJoueur1().getNbrJeuGagne()) + " - "
					+ String.valueOf(this.jeu.getJoueur2().getNbrJeuGagne());
			logger.info(this.score);
			this.jeu.initializeNbrGameWon();
	}

	public String getScore() {
		return score;
	}

	public void setScore(String score) {
		this.score = score;
	}

	public Game getJeu() {
		return jeu;
	}

	public void setJeu(Game jeu) {
		this.jeu = jeu;
	}

}
