package adbi.tennis.service;

import java.util.LinkedHashMap;
import java.util.Map;

import org.apache.log4j.Logger;

import adbi.tennis.entities.Player;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Match {
	final static Logger logger = Logger.getLogger(Match.class);
	private Map<String, String> sets;
	private Set set;

	public Match(Set set) {
		super();
		this.sets = new LinkedHashMap<String, String>();
		this.set = set;
	}

	public void playMatch() {
		int index=0;
		while (this.set.getJeu().getJoueur1().getNbrSetGagne() < 3 && this.set.getJeu().getJoueur2().getNbrSetGagne() < 3) {
			logger.info("********************* SET:" + String.valueOf(++index) + " *********************");
			this.set.playSet();
			sets.put("SET" + String.valueOf(index)+": ", this.set.getScore());
		}
	}

	public Player getWinner() {
		if (this.set.getJeu().getJoueur1().getNbrSetGagne()>this.set.getJeu().getJoueur2().getNbrSetGagne())
			return this.set.getJeu().getJoueur1();
		else
			return this.set.getJeu().getJoueur2();
	}

}
