package adbi.tennis.service;

import java.util.Random;
import java.util.Scanner;

import org.apache.log4j.Logger;

import adbi.tennis.entities.Player;
import lombok.Getter;
import lombok.Setter;


public class Game {
	final static Logger logger = Logger.getLogger(Game.class);
	private Player joueur1;
	private Player joueur2;
	private boolean isGameEnded;
	private boolean isTieBreakEnded;
	
	public Player getJoueur1() {
		return joueur1;
	}

	public void setJoueur1(Player joueur1) {
		this.joueur1 = joueur1;
	}

	public Player getJoueur2() {
		return joueur2;
	}

	public void setJoueur2(Player joueur2) {
		this.joueur2 = joueur2;
	}

	public boolean isGameEnded() {
		return isGameEnded;
	}

	public void setGameEnded(boolean isGameEnded) {
		this.isGameEnded = isGameEnded;
	}

	public boolean isTieBreakEnded() {
		return isTieBreakEnded;
	}

	public void setTieBreakEnded(boolean isTieBreakEnded) {
		this.isTieBreakEnded = isTieBreakEnded;
	}

	

	public Game(Player joueur1, Player joueur2) {
		this.joueur1 = joueur1;
		this.joueur2 = joueur2;
		this.isGameEnded = false;
		this.isTieBreakEnded = false;
	}

	//get Score for a tie break game
	public String getTieBreakScore() {
		if((joueur1.getTieBreakPoint()>=7 && joueur2.getTieBreakPoint()<=joueur1.getTieBreakPoint()-2) || (joueur2.getTieBreakPoint()>=7 && joueur1.getTieBreakPoint()<=joueur2.getTieBreakPoint()-2)) {
			winTieBreakGame(joueur1);
			return "_________________" + joueur1.getNom() + " gagne le jeu tie break" + "_________________ \n\n\n";
		}else if(joueur2.getTieBreakPoint()>=7 && joueur1.getTieBreakPoint()<=joueur2.getTieBreakPoint()-2) {
			winTieBreakGame(joueur2);
			return "_________________" + joueur2.getNom() + " gagne le jeu tie break" + "_________________ \n\n\n";
		}
		return joueur1.getNom()+ joueur1.getTieBreakPoint() + " - " + joueur2.getTieBreakPoint() + joueur2.getNom();
	}

	//get Score for a normal game
	public String getScore() {
		// Equality
		if (joueur1.getGameScore() == joueur2.getGameScore())
			switch (joueur1.getGameScore()) {
			case 0:
				return joueur1.getNom()+"   " + "0 - 0" + "   "+joueur2.getNom();
			case 1:
				return joueur1.getNom()+"   " + "15 - 15"+ "   "+joueur2.getNom();
			case 2:
				return joueur1.getNom()+"   " + "30 - 30"+ "   "+joueur2.getNom();
			case 3:
				return joueur1.getNom()+"   " + "40 - 40"+ "   "+joueur2.getNom();
			case 4:
				updateScoreAfterDeuce();
				return joueur1.getNom()+"   " + "40 - 40"+ "   "+joueur2.getNom();

			}
		// Deuce
		if (joueur1.getGameScore() >= 3 && joueur2.getGameScore() >= 3) {
			// Avantage to player1
			if (joueur1.getGameScore() == 4 && joueur2.getGameScore() == 3)
				return "AVANTAGE " + joueur1.getNom();
			// Avantage to player2
			if (joueur1.getGameScore() == 3 && joueur2.getGameScore() == 4)
				return "AVANTAGE " + joueur2.getNom();
			// Point + Avantage Player1
			if (joueur1.getGameScore() == 5 && joueur2.getGameScore() == 3) {
				winGame(joueur1);
				return "_________________" + joueur1.getNom() + "  a gagn� le jeu" + "_________________ \n\n\n";
			}
			// Point + Avantage Player2
			if (joueur1.getGameScore() == 3 && joueur2.getGameScore() == 5) {
				winGame(joueur2);
				return "_________________" + joueur2.getNom() + " a gagn� le jeu" + "_________________ \n\n\n";
			}
		} else {
			if (joueur1.getGameScore() == 4) {
				winGame(joueur1);
				return "_________________" + joueur1.getNom() + " a gagn� le jeu" + "_________________ \n\n\n";
			}
			if (joueur2.getGameScore() == 4) {
				winGame(joueur2);
				return "_________________" + joueur2.getNom() + " a gagn� le jeu" + "_________________ \n\n\n";
			}
		}
		return joueur1.getNom()+"   " +joueur1.getRealScore() + " - " + joueur2.getRealScore()+ "   "+joueur2.getNom();
	}

	public void updateScoreAfterDeuce() {
		joueur1.setGameScore(3);
		joueur2.setGameScore(3);
	}

	public void winGame(Player player) {
		this.isGameEnded = true;
		joueur1.setGameScore(0);
		joueur2.setGameScore(0);
		player.setNbrJeuGagne(player.getNbrJeuGagne() + 1);
	}

	public void winTieBreakGame(Player player) {
		this.isTieBreakEnded = true;
		joueur1.setNbrSetGagne(0);
		joueur2.setNbrSetGagne(0);
		player.setNbrJeuGagne(player.getNbrJeuGagne() + 1);
	}

	public void initializeNbrGameWon() {
		joueur1.setNbrJeuGagne(0);
		joueur2.setNbrJeuGagne(0);
	}

	public void initializeGameScore() {
		joueur1.setGameScore(0);
		joueur2.setGameScore(0);
	}

	public void whoIsThePlayerWinPoint() {
		int num = 0;
		/************* the user set a game result ********/
//		while(num!=1 && num!=2) {
//			System.out.println("Who win a point Player 1 or Player 2 (1 for player 1 and 2 for player 2)");
//			Scanner in = new Scanner(System.in);
//			num = in.nextInt();
//		}
		Random random = new Random();
		num = random.nextInt(2) + 1;
		switch (num) {
		case 1:
			this.joueur1.winPoint();
			break;
		case 2:
			this.joueur2.winPoint();
			break;
		}
	}

	public void WhoIsThePlayerWinTieBreak() {
		int num = 0;
		Random random = new Random();
		num = random.nextInt(2) + 1;
		switch (num) {
		case 1:
			this.joueur1.winTieBreakPoint();
			break;
		case 2:
			this.joueur2.winTieBreakPoint();
			break;
		}
	}

	public void playGame() {
		while (!this.isGameEnded) {
			whoIsThePlayerWinPoint();
			logger.info(getScore());
		}
		this.setGameEnded(false);
	}

	public void playTieBreakGame() {
		while (!this.isTieBreakEnded) {
			WhoIsThePlayerWinTieBreak();
			logger.info(getTieBreakScore());
		}
		this.setTieBreakEnded(false);
	}

}
