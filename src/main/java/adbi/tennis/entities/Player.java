package adbi.tennis.entities;

import lombok.Getter;
import lombok.Setter;


public class Player {

	private String nom;
	private int gameScore;
	private int nbrJeuGagne;
	private int nbrSetGagne;
	private int tieBreakPoint;

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public int getGameScore() {
		return gameScore;
	}

	public void setGameScore(int gameScore) {
		this.gameScore = gameScore;
	}

	public int getNbrJeuGagne() {
		return nbrJeuGagne;
	}

	public void setNbrJeuGagne(int nbrJeuGagne) {
		this.nbrJeuGagne = nbrJeuGagne;
	}

	public int getNbrSetGagne() {
		return nbrSetGagne;
	}

	public void setNbrSetGagne(int nbrSetGagne) {
		this.nbrSetGagne = nbrSetGagne;
	}

	public int getTieBreakPoint() {
		return tieBreakPoint;
	}

	public void setTieBreakPoint(int tieBreakPoint) {
		this.tieBreakPoint = tieBreakPoint;
	}

	public Player() {
		super();
	}

	public Player(String nom) {
		super();
		this.nom = nom;
		this.gameScore = 0;
		this.nbrJeuGagne = 0;
	}
	
	public void winPoint(){
		this.gameScore++;
	}
	
	public void winTieBreakPoint() {
		this.tieBreakPoint++;
	}

	public int getRealScore() {
		switch (this.gameScore) {
		case 0:
			return 0;
		case 1:
			return 15;
		case 2:
			return 30;
		default:
			return 40;
		}
	}

	@Override
	public String toString() {
		return "Player [name= " + nom + ", nombre de set gagn�s Sets=" + nbrSetGagne + "]";
	}

}
